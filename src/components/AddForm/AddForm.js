import React from 'react';
import uuid from 'uuid/v4';

import './AddForm.css';

class AddForm extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			inputText: ''
		};

		this.handleInputChange = this.handleInputChange.bind(this);
		this.handleAdd = this.handleAdd.bind(this);
	}

	handleInputChange(event) {
		this.setState({ inputText: event.target.value });
	}

	handleAdd(event) {
		event.preventDefault();
		
		const d = new Date();
		const task = {
			id: uuid(),
			taskText: this.state.inputText,
			creationDate: `${d.getDate()}/${d.getMonth() + 1}/${d.getFullYear()}`,
			completed: false
		};

		this.setState({ inputText: '' });
		this.props.onTaskAdd(task);
	}

	render() {
		return (
			<div className="add-form">
				<form onSubmit={this.handleAdd}>
					<input
						value={this.state.inputText}
						onChange={this.handleInputChange}
						placeholder="Enter your task here..." />
					<button type="submit">Add</button>
				</form>
			</div>
		);
	}
}

export default AddForm;