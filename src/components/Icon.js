import React from 'react';

const Icon = (props) => (
	<i className={"fa fa-" + props.name}></i>
);

export default Icon;