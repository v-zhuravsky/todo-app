import React from 'react';
import Icon from '../Icon';

import './ToDoItem.css';

const ToDoItem = (props) => (
	<div className="todo-item">
		<div className="todo-text">
			<h3 className={props.completed ? "task-done" : ""}>{props.taskText}</h3>
			<p>{props.creationDate}</p>
		</div>
		<button
			className="delete-task"
			onClick={() => props.onDelete(props.id)}>
			<Icon name="times" />
		</button>
		<button 
			className="complete-task"
			onClick={() => props.onComplete(props.index)}>
			<Icon name={props.completed ? "check-square-o" : "square-o"} />
		</button>
	</div>
);

export default ToDoItem;