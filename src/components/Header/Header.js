import React from 'react';
import './Header.css';

const Header = () => (
	<div className="header">
		<h1>ReactJS To Do App</h1>
	</div>
);

export default Header;