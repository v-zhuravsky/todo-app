import React from 'react';
import ToDoItem from '../ToDoItem/ToDoItem';

import './ToDoList.css';

const ToDoList = (props) => (
	<div className="todo-list">
		{
			props.tasks.map((task, i) => (
				<ToDoItem
					key={i}
					index={i}
					id={task.id}
					taskText={task.taskText}
					creationDate={task.creationDate}
					onDelete={props.onTaskDelete}
					onComplete={props.onTaskComplete}
					completed={task.completed} />
			))
		}
	</div>
);

export default ToDoList;