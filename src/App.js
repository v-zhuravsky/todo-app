import React, { Component } from 'react';

import Header from './components/Header/Header';
import ToDoList from './components/ToDoList/ToDoList';
import AddForm from './components/AddForm/AddForm';

class App extends Component {
	constructor(props) {
		super(props);

		let tasks = [];
		if (JSON.parse(localStorage.getItem('tasks')) !== null) {
			tasks = JSON.parse(localStorage.getItem('tasks'));
		}

		this.state = {
			tasks: tasks
		};

		this.handleTaskDelete = this.handleTaskDelete.bind(this);
		this.handleTaskAdd = this.handleTaskAdd.bind(this);
		this.handleTaskComplete = this.handleTaskComplete.bind(this);
	}

	handleTaskDelete(id) {
		const tasks = this.state.tasks.filter(task => task.id !== id);
		
		localStorage.setItem('tasks', JSON.stringify(tasks));
		this.setState({ tasks: JSON.parse(localStorage.getItem('tasks')) });
	}

	handleTaskAdd(task) {
		let tasks;
		if (localStorage.getItem('tasks')) {
			tasks = JSON.parse(localStorage.getItem('tasks'));
		} else {
			tasks = this.state.tasks;
		}
		tasks.push(task);

		localStorage.setItem('tasks', JSON.stringify(tasks));
		this.setState({ tasks: JSON.parse(localStorage.getItem('tasks')) });
	}

	handleTaskComplete(id) {
		let tasks = this.state.tasks;
		tasks[id].completed = tasks[id].completed ? false : true;

		localStorage.setItem('tasks', JSON.stringify(tasks));
		this.setState({ tasks: JSON.parse(localStorage.getItem('tasks')) });
	}

	render() {
		return (
			<div>
				<Header />
				{
					this.state.tasks.length === 0
						? <h1 className="no-tasks">You don't have any tasks</h1>
						: <ToDoList
							tasks={this.state.tasks}
							onTaskDelete={this.handleTaskDelete}
							onTaskComplete={this.handleTaskComplete} />
				}
				<AddForm onTaskAdd={this.handleTaskAdd} />
			</div>
		);
	}
}

export default App;
